package com.example.Tripal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class BudgetRecyclerViewAdapter extends RecyclerView.Adapter<BudgetRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "BudgetViewAdapter";

    private ArrayList<String> mCategoryNames;
    private ArrayList<Double> mSum;
    private Context mContext;
    private OnItemListener mOnItemListener;




    public BudgetRecyclerViewAdapter(ArrayList<String> mCategoryNames, ArrayList<Double> mSum, Context mContext, OnItemListener onItemListener) {
        this.mCategoryNames = mCategoryNames;
        this.mSum = mSum;
        this.mContext = mContext;
        this.mOnItemListener = onItemListener;
    }

    @NonNull
    @Override
    public BudgetRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.budget_list_item,parent,false);
        ViewHolder holder = new ViewHolder(view, mOnItemListener);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull BudgetRecyclerViewAdapter.ViewHolder holder, final int position) {
             holder.categoryName.setText(mCategoryNames.get(position));
             holder.categorySum.setText(Double.toString(mSum.get(position)) + " €");
    }

    @Override
    public int getItemCount() {
        return mCategoryNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
         TextView categoryName;
         TextView categorySum;
         RelativeLayout parentLayout;
         OnItemListener onItemListener;

        public ViewHolder(View itemView, OnItemListener onItemListener) {
            super(itemView);

            categoryName = itemView.findViewById(R.id.budget_category);
            categorySum = itemView.findViewById(R.id.budget_category_sum);
            parentLayout = itemView.findViewById(R.id.budget_parent_layout);
            this.onItemListener = onItemListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onItemListener.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemListener{
        void onItemClick(int position);
    }
}



