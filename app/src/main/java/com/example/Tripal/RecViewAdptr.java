package com.example.Tripal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RecViewAdptr extends RecyclerView.Adapter<RecViewAdptr.ViewHolder> {
    private static final String TAG = "RecViewAdptr";


//    Pole veel kindel, kas image'id lähevad ka database'i
//    private ArrayList<String> mImages = new ArrayList<>();
    private ArrayList<String> mImageNames = new ArrayList<>();
    private ArrayList<String> mCont;
    private Context mContext;
    private OnNoteListener mOnNoteListener;

    public RecViewAdptr (Context context, ArrayList<String> cont, ArrayList<String> imageNames, OnNoteListener onNoteListener) {
        mImageNames = imageNames; // Vb peaks teised nimed olema (vastavalt note_list.xml nimele image_note)
//        Not sure about those images...
//        mImages = images;
        mCont = cont;
        mContext = context;
        this.mOnNoteListener = onNoteListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_list, parent, false);
        ViewHolder holder = new ViewHolder(view, mOnNoteListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");

        Glide.with(mContext)
                .asBitmap()
                .load(mImageNames.get(position))
                .into(holder.image);

        holder.imageName.setText(mImageNames.get(position));

//        holder.layout_note.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick (View view) {
//                Log.d(TAG, "onClick: clicked on: " + mImageNames.get(position));
//
//                Toast.makeText(mContext, mImageNames.get(position), Toast.LENGTH_SHORT).show();
//
//                // Siia lisada note nähtav osa! (saab näha, mis on note'i kirjutatud)
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return mImageNames.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        OnNoteListener onNoteListener;
        ImageView image;
        TextView imageName;
        RelativeLayout layout_note;

        public ViewHolder(@NonNull View itemView, OnNoteListener onNoteListener) {
            super(itemView);
            image = itemView.findViewById(R.id.image_note);
            imageName = itemView.findViewById(R.id.noteName);
            layout_note = itemView.findViewById(R.id.rel_layout_note);
            this.onNoteListener = onNoteListener;
            Log.d(TAG, "ViewHolder : public ViewHolder");
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View view) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    public interface OnNoteListener {
        void onNoteClick(int position);
    }
}
