package com.example.Tripal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

// FIXME: Has to handle somewhere if two trips are created with same name

public class DBHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "trips.db";
    // Table names
    private static final String TABLE_MY_TRIPS = "my_trips";
    private static final String TABLE_BUDGET_ITEMS = "budget_items";
    private static final String TABLE_BUDGET_CATEGORIES = "budget_categories";
    private static final String TABLE_GALLERY_PICTURES = "gallery_pictures";
    private static final String TABLE_NOTES = "my_notes";

    // Common column names
    private static final String COLUMN_ID = "id";

    // MY_TRIPS Table - column names
    private static final String COLUMN_TRIP_NAME = "trip_name";
    private static final String COLUMN_TRIP_DATE = "trip_date";
    private static final String COLUMN_PICTURE_URI = "trip_picture";

    // BUDGET_ITEMS Table - column names
    private static final String COLUMN_ITEM = "item";
    private static final String COLUMN_SUM = "sum";
    private static final String COLUMN_TRIP_ID = "trip_id";
    private static final String COLUMN_ITEM_CATEGORY_ID = "item_category_id";

    // BUDGET_CATEGORIES Table - column names
    private static final String COLUMN_CATEGORY_NAME = "category_name";
    private static final String COLUMN_CATEGORY_TRIP_ID = "category_trip_id";

    // GALLERY_PICTURES Table - column names
    private static final String COLUMN_GALLERY_TRIP_ID = "gallery_trip_id";
    private static final String COLUMN_GALLERY_PICTURE_URI = "gallery_picture_uri";

    // TABLE_NOTES Table - column names
    public static final String COLUMN_NOTES_TRIP_ID = "id_trip";
    public static final String COLUMN_NOTE_ID = "id_notes";
    public static final String COLUMN_NOTE_NAME = "notes_name";
    public static final String COLUMN_NOTE_CONTEXT = "notes_context";

    // MY_TRIPS table create statement
    private static final String CREATE_TABLE_MY_TRIPS = "CREATE TABLE " + TABLE_MY_TRIPS + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_TRIP_NAME + "  TEXT NOT NULL," +
            COLUMN_TRIP_DATE + " TEXT NOT NULL," +
            COLUMN_PICTURE_URI + " TEXT" +
            ");";

    // BUDGET_ITEMS table create statement
    private static final String CREATE_TABLE_BUDGET_ITEMS = "CREATE TABLE " + TABLE_BUDGET_ITEMS + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_ITEM + "  TEXT NOT NULL," +
            COLUMN_SUM + " REAL NOT NULL," +
            COLUMN_TRIP_ID + " INTEGER NOT NULL," +
            COLUMN_ITEM_CATEGORY_ID + " INTEGER NOT NULL, " +
            "FOREIGN KEY(" + COLUMN_TRIP_ID + ") REFERENCES " + TABLE_MY_TRIPS +
            "(" + COLUMN_TRIP_ID  + "), " +
            "FOREIGN KEY(" + COLUMN_ITEM_CATEGORY_ID + ") REFERENCES " + TABLE_BUDGET_CATEGORIES +
            "(" + COLUMN_ID  + ") " +
            ");";

    // BUDGET_CATEGORIES table create statement
    private static final String CREATE_TABLE_BUDGET_CATEGORIES = "CREATE TABLE " + TABLE_BUDGET_CATEGORIES + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_CATEGORY_NAME + "  TEXT NOT NULL, " +
            COLUMN_CATEGORY_TRIP_ID + " INTEGER NOT NULL, " +
            "FOREIGN KEY(" + COLUMN_CATEGORY_TRIP_ID + ") REFERENCES " + TABLE_MY_TRIPS +
            "(" + COLUMN_TRIP_ID  + ") " +
            ");";

    // GALLERY_PICTURES table create statement
    private static final  String CREATE_GALLERY_PICTURES = "CREATE TABLE " + TABLE_GALLERY_PICTURES + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_GALLERY_PICTURE_URI + " TEXT NOT NULL, " +
            COLUMN_GALLERY_TRIP_ID + " INTEGER NOT NULL, " +
            "FOREIGN KEY(" + COLUMN_GALLERY_TRIP_ID + ") REFERENCES " + TABLE_MY_TRIPS +
            "(" + COLUMN_TRIP_ID  + ") " +
            ");";

    // TABLE_NOTES table create statement
    private static final String CREATE_TABLE_NOTES = "CREATE TABLE " + TABLE_NOTES + "(" +
            COLUMN_NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_NOTE_NAME + " TEXT NOT NULL, " +
            COLUMN_NOTE_CONTEXT + " TEXT, " +
            COLUMN_TRIP_ID + " INTEGER NOT NULL, " +
            "FOREIGN KEY (" + COLUMN_TRIP_ID + ") REFERENCES " + TABLE_MY_TRIPS +
            "(" + COLUMN_ID + ") " +
            ");";


    public DBHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREATE_TABLE_MY_TRIPS);
        sqLiteDatabase.execSQL(CREATE_TABLE_BUDGET_ITEMS);
        sqLiteDatabase.execSQL(CREATE_TABLE_BUDGET_CATEGORIES);
        sqLiteDatabase.execSQL(CREATE_GALLERY_PICTURES);
        sqLiteDatabase.execSQL(CREATE_TABLE_NOTES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_MY_TRIPS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_BUDGET_ITEMS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_BUDGET_CATEGORIES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES + ";");
        onCreate(sqLiteDatabase);
    }



    /* FUNCTIONS RELATED TO MY_TRIPS TABLE */


    public void addTrip (Trip trip){
        Log.d("SQL ", "function addTrip. keeps crashing");
        ContentValues values = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();


        values.put(COLUMN_TRIP_NAME, trip.getTripName());
        values.put(COLUMN_TRIP_DATE, trip.getTripDates());
        values.put(COLUMN_PICTURE_URI, trip.getTripPicture());

        db.insert(TABLE_MY_TRIPS,null, values);
        db.close();
    }


    public ArrayList getAllTrips (){
        String selectQuery = "SELECT * FROM " + TABLE_MY_TRIPS;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery,null,null);
        ArrayList tripArrayList = new ArrayList();
        if (cursor.moveToFirst()){
            do {
                Trip trip = new Trip("","","");
                trip.setTripName(cursor.getString(cursor.getColumnIndex(COLUMN_TRIP_NAME)));
                trip.setTripDates(cursor.getString(cursor.getColumnIndex(COLUMN_TRIP_DATE)));
                trip.setTripPicture(cursor.getString(cursor.getColumnIndex(COLUMN_PICTURE_URI)));

                tripArrayList.add(trip);
            }while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return tripArrayList;
    }

    public int getTripId(String tripName){
        String query = ("SELECT " + COLUMN_ID + " FROM " + TABLE_MY_TRIPS + " WHERE " + COLUMN_TRIP_NAME + " = \"" + tripName + "\";");
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null,null);
        cursor.moveToFirst();
        Integer temp = cursor.getInt(0);
        cursor.close();
        db.close();
        return temp;
    }

    public void updateTripPicture (int id, String imageUri){

        ContentValues values = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();

        values.put(COLUMN_PICTURE_URI, imageUri);

        db.update(TABLE_MY_TRIPS,values, "id="+id, null);
        db.close();
        db.close();
    }

    public String getImage(int i){
        SQLiteDatabase db = this.getWritableDatabase();

        String qu = "select " + COLUMN_PICTURE_URI  + "  from " +
                TABLE_MY_TRIPS +  "  where " + COLUMN_ID + "=" +  i ;
        Cursor cur = db.rawQuery(qu, null);

        if (cur.moveToFirst()){
           String temp = cur.getString(cur.getColumnIndex(COLUMN_PICTURE_URI));
            cur.close();
            return temp;
        }
        if (cur != null && !cur.isClosed()) {
            cur.close();
        }
        db.close();

        return null ;
    }


    public void deleteTrip(Trip trip){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_MY_TRIPS + " WHERE " + COLUMN_TRIP_NAME + "=\"" + trip.getTripName() + "\";");
    }

    public String getTripName (Integer id){
        SQLiteDatabase db = this.getReadableDatabase();

        String qu = "SELECT " + COLUMN_TRIP_NAME  + "  FROM " +
                TABLE_MY_TRIPS +  "  where " + COLUMN_ID + "=" +  id ;
        Cursor cur = db.rawQuery(qu, null);

        if (cur.moveToFirst()){
            String temp = cur.getString(cur.getColumnIndex(COLUMN_TRIP_NAME));
            cur.close();
            return temp;
        }
        if (cur != null && !cur.isClosed()) {
            cur.close();
        }
        db.close();

        return null ;
    }



    /* FUNCTIONS RELATED TO BUDGET CATEGORIES TABLE */



    public ArrayList getAllTripBudgetCategories (int tripId){
        String query = "SELECT " + COLUMN_CATEGORY_NAME + " FROM " + TABLE_BUDGET_CATEGORIES +
                " WHERE " + COLUMN_CATEGORY_TRIP_ID + " = " + tripId + ";";

        ArrayList<String> categories = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY_NAME));

                categories.add(name);
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();

        return categories;
    }

    public void initialiseBudgetCategories (int tripId){
        SQLiteDatabase db = this.getWritableDatabase();
        String query =  "INSERT INTO " + TABLE_BUDGET_CATEGORIES +
                " (" + COLUMN_CATEGORY_NAME +",  "+ COLUMN_CATEGORY_TRIP_ID +") VALUES ('Transport'," + tripId + "), ('Food'," + tripId +  "), ('Accommodation'," + tripId + "), ('Sightseeings',"+ tripId + ");";
        db.execSQL(query);
        db.close();
    }

    public String getCategoryName(Integer tripId, Integer categoryId){
        String query = "SELECT " + COLUMN_CATEGORY_NAME  + " FROM " + TABLE_BUDGET_CATEGORIES +
                " WHERE " + COLUMN_CATEGORY_TRIP_ID + " = " + tripId + " AND " + COLUMN_ID +  " = " +
                categoryId +  ";";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery(query, null);

        if (cur.moveToFirst()){
            String temp = cur.getString(cur.getColumnIndex(COLUMN_CATEGORY_NAME));
            cur.close();
            return temp;
        }
        if (cur != null && !cur.isClosed()) {
            cur.close();
        }
        db.close();

        return null ;
    }

    public void addTripBudgetCategory (String category, int tripId) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();


        values.put(COLUMN_CATEGORY_NAME, category);
        values.put(COLUMN_CATEGORY_TRIP_ID, tripId);


        db.insert(TABLE_BUDGET_CATEGORIES,null, values);
        db.close();
    }


    /* FUNCTIONS RELATED TO BUDGET ITEMS TABLE */

    public Map getTripBudgetItems (int tripId, int categoryId){
        String query = "SELECT " + COLUMN_ITEM + ", " + COLUMN_SUM  + " FROM " + TABLE_BUDGET_ITEMS +
                " WHERE " + COLUMN_TRIP_ID + " = " + tripId + " AND " +
                COLUMN_ITEM_CATEGORY_ID + " = " + categoryId + ";";

        Map<String, Double> map =  new HashMap<String, Double>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_ITEM));
                Double price = cursor.getDouble(cursor.getColumnIndex(COLUMN_SUM));

                map.put(name,price);
                cursor.moveToNext();
            }
        }

        cursor.close();
        db.close();

        return map;
    }

    public void addTripBudgetItem (String itemName, String price, int tripId, int categoryId){
        ContentValues values = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();


        values.put(COLUMN_ITEM, itemName);
        values.put(COLUMN_SUM, price);
        values.put(COLUMN_TRIP_ID, tripId);
        values.put(COLUMN_ITEM_CATEGORY_ID, categoryId);


        db.insert(TABLE_BUDGET_ITEMS,null, values);
        db.close();


    }

    /* FUNCTIONS RELATED FINDING SUMS */


    public Double getCategorySum (Integer tripId, Integer categoryId){
        String query = "SELECT " + COLUMN_SUM  + " FROM " + TABLE_BUDGET_ITEMS +
                " WHERE " + COLUMN_TRIP_ID + " = " + tripId + " AND " +
                COLUMN_ITEM_CATEGORY_ID + " = " + categoryId + ";";

        Double categorySum = 0.0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Double price = cursor.getDouble(cursor.getColumnIndex(COLUMN_SUM));

                categorySum += price;
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();

        return categorySum;
    }



    public double getTripSum (Integer tripId){
        String query = "SELECT " + COLUMN_SUM  + " FROM " + TABLE_BUDGET_ITEMS +
                " WHERE " + COLUMN_TRIP_ID + " = " + tripId +  ";";

        Double tripSum = 0.0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Double price = cursor.getDouble(cursor.getColumnIndex(COLUMN_SUM));

                tripSum += price;
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();

        return tripSum;
    }


    /* FUNCTIONS RELATED TO GALLERY_PICTURES */

    public void addGalleryPictures (int tripId, String filepath){
        ContentValues values = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();

        values.put(COLUMN_GALLERY_PICTURE_URI,filepath);
        values.put(COLUMN_GALLERY_TRIP_ID,tripId);

        db.insert(TABLE_GALLERY_PICTURES,null, values);
        db.close();
    }

    public ArrayList<String> getAllGalleryPictures (int tripId){
        String query = "SELECT " + COLUMN_GALLERY_PICTURE_URI + " FROM " + TABLE_GALLERY_PICTURES +
                " WHERE " + COLUMN_GALLERY_TRIP_ID + " = " + tripId + ";";

        ArrayList<String> pictureUries = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String uri = cursor.getString(cursor.getColumnIndex(COLUMN_GALLERY_PICTURE_URI));

                pictureUries.add(uri);
                cursor.moveToNext();
            }
        }

        cursor.close();
        db.close();

        return pictureUries;

    }

    public Integer getGalleryPictureId (String pictureUri){
        String query = "SELECT " + COLUMN_ID  + " FROM " + TABLE_GALLERY_PICTURES +
                " WHERE " + COLUMN_GALLERY_PICTURE_URI + " = " + "'" + pictureUri +  "';";

        Integer pictureId = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                pictureId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));

                cursor.moveToNext();
            }
        }

        cursor.close();
        db.close();

        return pictureId;

    }

    public String getGalleryPictureById(Integer id){
        String query = "SELECT " + COLUMN_GALLERY_PICTURE_URI  + " FROM " + TABLE_GALLERY_PICTURES +
                " WHERE " + COLUMN_ID + " = "  + id + ";";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String pictureUri = null;
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                pictureUri = cursor.getString(cursor.getColumnIndex(COLUMN_GALLERY_PICTURE_URI));

                cursor.moveToNext();
            }
        }

        cursor.close();
        db.close();

        return pictureUri;


    }

    // Functions related to notes table

    public void addNote (Note note, int tripId) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();

        values.put(COLUMN_NOTE_NAME, note.getNoteName());
        values.put(COLUMN_TRIP_ID, tripId);

        db.insert(TABLE_NOTES, null, values);
        db.close();
    }

    public void updateNoteContent (String content, String noteId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_NOTE_CONTEXT, content);
        sqLiteDatabase.update(TABLE_NOTES, values, "id_notes = ?", new String [] {noteId});
        sqLiteDatabase.close();

//        sqLiteDatabase.execSQL("UPDATE " + TABLE_NOTES +
//                "SET " + COLUMN_NOTE_CONTEXT + " = " + content +
//                "WHERE " + COLUMN_NOTE_ID + " = " + noteId + ";");
    }

    public ArrayList getAllNotes (int tripID) {
//        Log.d(TAG, "getAllNotes : getting all notes");
        String selectQuery = "SELECT * FROM " + TABLE_NOTES + " WHERE " + COLUMN_TRIP_ID + " = " + tripID + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db. rawQuery (selectQuery, null, null);
        ArrayList noteArrayList = new ArrayList();
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note ("");
                note.setNoteName(cursor.getString(cursor.getColumnIndex(COLUMN_NOTE_NAME)));
                note.setNoteContext(cursor.getString(cursor.getColumnIndex(COLUMN_NOTE_CONTEXT)));

                noteArrayList.add(note);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
//        Log.d(TAG, "getAllNotes : got all notes!");
        return noteArrayList;
    }

    public int getNoteId (String noteName) {
        String query = ("SELECT " + COLUMN_NOTE_ID + " FROM " + TABLE_NOTES + " WHERE " + COLUMN_NOTE_NAME + " = \"" + noteName + "\";");
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null, null);
        cursor.moveToFirst();

        return cursor.getInt(0);
    }

    public String getNoteContext (int noteID) {
        String context = "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + COLUMN_NOTE_CONTEXT + " FROM " + TABLE_NOTES +
                " WHERE " + COLUMN_NOTE_ID + " = " + noteID + ";", null);
        cursor.moveToFirst();
        context = context + cursor.getString(cursor.getColumnIndex("notes_context"));
        cursor.close();
        return context;
    }
}

