package com.example.Tripal;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import java.io.File;

public class DisplayPictureDetails extends AppCompatActivity {
    private DBHandler db;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.db = new DBHandler(this, null, null);
        setContentView(R.layout.image_details);



        Intent intent = getIntent();
        Integer pictureId = intent.getIntExtra("imageId", -1);

        File imgFile = new File(db.getGalleryPictureById(pictureId));

        ImageView image = findViewById(R.id.gallery_detail_view);

        image.setImageBitmap(getBitmap(imgFile));

//        Integer image = getIntent().getIntExtra("image", 1);
//
//        ImageView imageView = findViewById(R.id.gallery_detail_view);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        imageView.setImageResource(image);


    }


    private Bitmap getBitmap(File imgFile) {
        Bitmap myBitmap = null;
        String[] permissions = new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (imgFile.exists()) {
            final int MyVersion = Build.VERSION.SDK_INT;
            if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (!checkIfHavePermission()) {
                    ActivityCompat.requestPermissions(this, permissions, 1);
                } else {
//                    myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath()); //TODO: Have to check if we even need this part

                }

                myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath()); // TODO: Have to check it doesn't crash without permission
            }
        }

        return myBitmap;
    }

    private boolean checkIfHavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

}

