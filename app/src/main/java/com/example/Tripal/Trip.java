package com.example.Tripal;

public class Trip {
    private String TripName;
    private String TripDates;
    private String TripPicture;

    public Trip(String tripName, String tripDates, String tripPicture) {
        TripName = tripName;
        TripDates = tripDates;
        TripPicture = tripPicture;
    }

    public String getTripName () {return TripName;}
    public String getTripDates () {return TripDates;}
    public String getTripPicture () {return TripPicture;}

    public void setTripName(String name){
        TripName = name;
    }
    public void setTripDates(String dates){
        TripDates = dates;
    }
    public void setTripPicture(String picture){
        TripPicture = picture;
    }
}
