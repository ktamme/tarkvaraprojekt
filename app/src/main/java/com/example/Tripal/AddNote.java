package com.example.Tripal;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class AddNote extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "AddNote";
    private EditText noteTitle;
    private Integer tripId;
    private DBHandler db;
    Note note;

    @Override
    protected void onCreate (@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.note_add);
        noteTitle = findViewById(R.id.new_note_title);
        ImageView backspace = findViewById(R.id.note_content_backspace);
        findViewById(R.id.finish_note_button).setOnClickListener(this);
        Log.d(TAG, "onCreate: AddNote onCreate started.");
        Intent intent = getIntent();
        this.tripId = intent.getIntExtra("TripId", -1);
        db = new DBHandler(this, null, null);
        super.onCreate(savedInstanceState);


        backspace.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.note_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public String getNoteName () { return noteTitle.getText().toString();}

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.finish_note_button:
                Intent intent = new Intent(AddNote.this, NoteMainAct.class);
                note = new Note(this.getNoteName());
                db.addNote(note, tripId);

                startActivity(intent);
                Log.d(TAG, "onClick : note added");
                break;
        }
    }
}
