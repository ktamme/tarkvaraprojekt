package com.example.Tripal;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

public class TripInitialPage extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "TripInitialPage";
    private static final int PICK_IMAGE = 100;
    private int tripId;
    private ImageView tripProfilePic;
    private ImageView tripBudget;
    private ImageView tripMemos;
    private ImageView tripPictures;
    private Button add_trip_picture;
    private DBHandler db;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.db = new DBHandler(this, null, null);
        setContentView(R.layout.trip_initial_page);

        Intent intent = getIntent();
        this.tripId = intent.getIntExtra("TripId", -1);
        if (tripId == -1){
            this.finishAffinity(); //  If didn't get tripId then close application and all activities
        }


        Toolbar toolbar = findViewById(R.id.initial_page_tool_bar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        ImageView backspace = toolbar.findViewById(R.id.tool_bar_backspace);
        mTitle.setText(db.getTripName(tripId));
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayShowHomeEnabled(true);
//        actionBar.setDisplayHomeAsUpEnabled(true);

        tripProfilePic = findViewById(R.id.trip_profile_image);
        tripBudget = findViewById(R.id.trip_budget);
        tripMemos = findViewById(R.id.trip_memos);
        tripPictures = findViewById(R.id.trip_pictures);
        add_trip_picture = findViewById(R.id.add_trip_picture);

        tripBudget.setOnClickListener(this);
        tripMemos.setOnClickListener(this);
        tripPictures.setOnClickListener(this);
        add_trip_picture.setOnClickListener(this);

        File imgFile = new File(db.getImage(this.tripId));
        Log.d(TAG, "Andmebaasist saamine bitmapi jaoks " + db.getImage(this.tripId));
        String[] permissions = new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE};

        tripProfilePic.setImageBitmap(getBitmap(imgFile, permissions));

        Log.d(TAG, "Bitmapi saamine " + getBitmap(imgFile, permissions));

        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TripInitialPage.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private boolean checkIfHavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {
            case R.id.trip_budget:
                Intent budgetIntent = new Intent(TripInitialPage.this, TripBudget.class);
                budgetIntent.putExtra("TripId", tripId);

                startActivity(budgetIntent);
                break;
            case R.id.trip_memos:
                Intent memosIntent = new Intent(TripInitialPage.this, NoteMainAct.class);
                startActivity(memosIntent);
                break;
            case R.id.trip_pictures:
                Intent picturesIntent = new Intent(TripInitialPage.this, TripPictures.class);
                picturesIntent.putExtra("TripId", tripId);
                startActivity(picturesIntent);
                break;
            case R.id.add_trip_picture:
                openGallery();

                break;
        }
    }

    private void openGallery() {
//        Intent gallery = new Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
//        startActivityForResult(gallery,PICK_IMAGE);

        Intent galleryIntent = new Intent(Intent.ACTION_PICK);
        galleryIntent.setType("image/*, video/*");
        if (galleryIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(galleryIntent, "Select File"), PICK_IMAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try
        {
            if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {

                String filepath;
                Uri _uri = data.getData();
                Log.d("", "URI = " + _uri);
                if (_uri != null && "content".equals(_uri.getScheme())) {
                    Cursor cursor = this.getContentResolver().query(_uri, new String[]{android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                    cursor.moveToFirst();
                    filepath = cursor.getString(0);
                    cursor.close();
                } else {
                    filepath = _uri.getPath();
                }


                db.updateTripPicture(this.tripId, filepath);
//
//                Toast.makeText(this, data.getData().toString(),
//                        Toast.LENGTH_LONG).show();

                Log.d(TAG, "Pildi salvestamine db: " + filepath);
                Log.d(TAG, "data.getdata v2ljund: " + data.getData());
                tripProfilePic.setImageURI(data.getData());
            }
        }catch (Exception e)
        {
            this.finishAffinity();
        }
    }

    private Bitmap getBitmap(File imgFile, String[] permissions) {
        Bitmap myBitmap = null;
        if (imgFile.exists()) {
            final int MyVersion = Build.VERSION.SDK_INT;
            if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (!checkIfHavePermission()) {
                    ActivityCompat.requestPermissions(this, permissions, 1);
                } else {
//                    myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath()); //TODO: Have to check if we even need this part

                }

                myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath()); // TODO: Have to check it doesn't crash without permission
            }
        }

        return myBitmap;
    }


}
