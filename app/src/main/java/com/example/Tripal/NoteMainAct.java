package com.example.Tripal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class NoteMainAct extends AppCompatActivity implements RecViewAdptr.OnNoteListener {
    private static final String TAG = "NoteMainAct";
    private int tripID;

    private Toolbar toolbar;
    private MenuItem menuItem;
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mCont = new ArrayList<>();
    private DBHandler db;

//    Jällegi pole kindel, kas image läheb database'i?
//    private ArrayList<String> mImageLoc = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_activity_main);
        Log.d(TAG, "onCreate: started.");
        ImageView backspace = findViewById(R.id.note_content_backspace);
        db = new DBHandler(this,null,null);
        toolbar = findViewById(R.id.toolbar_content);
//        toolbar.findViewById(R.id.notes_page_title) = (TextView) "Your notes";
//        toolbar.setTitle("Your notes");
        menuItem = findViewById(R.id.new_note_button);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        this.tripID = intent.getIntExtra("TripId", -1);
        initNotes(); // Initializes notes


        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.note_main, menu);
        return true;
    }

    @Override // Laseb uue kirje lisada
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.new_note_button:
                startActivity(new Intent( this, AddNote.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initNotes() {
        Log.d(TAG, "initNotes: Preparing notes");

        parseNotesList(db.getAllNotes(tripID));
        initRecyclerView();
    }

    private void parseNotesList (ArrayList<Note> noteArrayList) {
//        Pole kindel kas vaja
//        See tundub ainult image'itega seotud olevat
//        String[] permissions = new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE};
        Log.d(TAG, "parseNotesList : Parsing notes");
        for (Note temp : noteArrayList) {
            mNames.add(temp.getNoteName());
            mCont.add(temp.getNoteContext());

//            Mingi image'itega teema
//            -- vist pole vaja, kui image on sama kõigil
//            mImageUrls.add(getBitmap(new File(temp.getTripPicture()), permissions));
        }
    }

    private void initRecyclerView () {
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        // Siia sisestada, millega ära täita note_list osad (image ise ja kiri image kõrvale)
         RecViewAdptr adapter = new RecViewAdptr(this, mCont, mNames, this);

        recyclerView.setAdapter(adapter); // Käivitab adapteri (täidab ära recyclerview)
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Log.d(TAG, "initRecyclerView: recyclerView initiated");
    }

    @Override
    public void onNoteClick(int position) {
        mNames.get(position);
        Intent intent = new Intent (this, NoteContent.class);
        intent.putExtra("NoteID", db.getNoteId(mNames.get(position)));
        Log.d(TAG, "onNoteClick : note clicked");
        startActivity(intent);
    }
}
