package com.example.Tripal;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class AddTrip extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "AddTrip";
    private EditText tripName;
    private EditText tripDates;
    Button finishAddingTrip;
    DBHandler db;
    Trip trip;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_trip);
        tripName = findViewById(R.id.new_trip_name);
        tripDates = findViewById(R.id.trip_dates);
        finishAddingTrip = findViewById(R.id.finish_adding_trip);

        Toolbar toolbar = findViewById(R.id.add_trip_tool_bar);
        ImageView backspace = toolbar.findViewById(R.id.tool_bar_backspace);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("New Trip");

        db = new DBHandler(this,null,null);

        finishAddingTrip.setOnClickListener(this);

        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddTrip.this,MainActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public String getURLForResource (int resourceId) {
        return Uri.parse("android.resource://"+R.class.getPackage().getName()+"/" +resourceId).toString();
    }


    public String getTripName (){
        return tripName.getText().toString();
    }

    public String getTripDates (){
        return tripDates.getText().toString();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.finish_adding_trip:
                Intent intent = new Intent(AddTrip.this,MainActivity.class);
                trip = new Trip(this.getTripName(), this.getTripDates(), getURLForResource(R.drawable.landscape_icon)); // Uri.parse("android.resource://com.example.tarkvaraprojekt/" + R.drawable.ic_launcher_background).toString()
                Log.d(TAG, "adding item picture path " + getURLForResource(R.drawable.landscape_icon));
                db.addTrip(trip);
                startActivity(intent);

                break;

        }



    }
}
