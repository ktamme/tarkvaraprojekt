package com.example.Tripal;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;




public class MainActivity extends AppCompatActivity implements RecyclerViewAdapter.OnTripListener {
    private static final String TAG = "MainActivity";

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<Bitmap> mImageUrls = new ArrayList<>();
    private ArrayList<String> mTripDates = new ArrayList<>();
    private DBHandler db;
    private FloatingActionButton add_trip;
    private RecyclerViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: started.");   // For debugging

        db = new DBHandler(this,null,null);
        add_trip = findViewById(R.id.add_new_trip);


        Toolbar toolbar = findViewById(R.id.tool_bar);
        ImageView backspace = toolbar.findViewById(R.id.tool_bar_backspace);
        backspace.setVisibility(View.INVISIBLE);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("My Travels");



        findViewById(R.id.new_trip);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        initImageBitmaps(); //  Initializes all images put on recyclerview

        add_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,AddTrip.class));
            }
        });

    }

    private void initImageBitmaps (){
        Log.d(TAG,"initImageBitmaps; preparing bitmaps.");

/*         GOOD TO USE FOR SOME QUICK DEBUGGING
            Toast.makeText(this, "TOAST",
                    Toast.LENGTH_LONG).show();*/

        mNames.clear();
        mImageUrls.clear();
        mTripDates.clear();

        parseTripArrayList(db.getAllTrips());
        initRecyclerView();
    }

    /*
    Have to reinitialise recyclerview after user has selected/changed profile picture for trip.
    Usually should be done in onResume(), but because of a bug in AndroidSDK, onResume() what should be
    called once is called constantly. Therefore needed to bypass this.

    Solution is to finish intent in onNewIntent() every time when new intent is called. And from
    addTrip and TripInitialPage new intent to MainActivity is started instead of just finishing old intent
     */
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.finish();
    }

    private void initRecyclerView (){
        Log.d(TAG, "initRecyclerView: init recyclerview.");

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        adapter = new RecyclerViewAdapter(this,mNames,mImageUrls, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    private void parseTripArrayList(ArrayList<Trip> tripArrayList) {
        String[] permissions = new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE};
        for (Trip temp : tripArrayList) {
            mNames.add(temp.getTripName());
            mTripDates.add(temp.getTripDates());


            Log.d(TAG, "Tripi parsimine: " + temp.getTripPicture());
            mImageUrls.add(getBitmap(new File(temp.getTripPicture()),permissions));

        }
    }


    @Override
    public void onTripClick(int position) {
        mNames.get(position);
        Intent intent = new Intent(this,TripInitialPage.class);
//        Toast.makeText(this, String.valueOf(db.getTripId(mNames.get(position))),
//                Toast.LENGTH_LONG).show();
        intent.putExtra("TripId", db.getTripId(mNames.get(position)));



        startActivity(intent);
    }

    public Bitmap getBitmap(File imgFile, String[] permissions) {
        Bitmap myBitmap = null;
        if (imgFile.exists()) {
            final int MyVersion = Build.VERSION.SDK_INT;
            if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (!checkIfHavePermission()) {
                    ActivityCompat.requestPermissions(this, permissions, 1);
                } else {
//                    myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath()); //TODO: Have to check if we even need this part
                }

                myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());  // TODO: Have to check it doesn't crash without permission
            }
        }

        return myBitmap;
    }

    private boolean checkIfHavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}
