package com.example.Tripal;

public class Note {
    private String noteName;
    private String noteContext;

    public Note(String note_name) {
        noteName = note_name;
    }

    public String getNoteName () { return noteName; }
    public String getNoteContext() { return noteContext; }

    public void setNoteName (String name) { noteName = name; }
    public void setNoteContext (String context ) { noteContext = context; }
}
