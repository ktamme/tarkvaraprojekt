package com.example.Tripal;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class TripPictures extends AppCompatActivity {
    private static final int PICK_IMAGE = 100;
    GridView gridView;
    private Toolbar toolbar;
    private TextView mTitle;
    private DBHandler db;
    private int tripId;
    private final static String TAG = "GALLERY";
    String imageEncoded;
    List<String> imagesEncodedList;

    private ArrayList<String> mImages = new ArrayList<>();
    private ArrayList<Bitmap> pictureBitmaps = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.db = new DBHandler(this, null, null);
        setContentView(R.layout.gallery);

        Intent intent = getIntent();
        this.tripId = intent.getIntExtra("TripId", -1);

        toolbar = findViewById(R.id.gallery_tool_bar);
        mTitle = findViewById(R.id.toolbar_title);
        ImageView backspace = toolbar.findViewById(R.id.tool_bar_backspace);
        mTitle.setText("Gallery");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        pictureBitmaps = initializePicArray();

        gridView = findViewById(R.id.grid_gallery);
        gridView.setStretchMode(GridView.NO_STRETCH);
        GridAdapter gridAdapter = new GridAdapter(this, pictureBitmaps);

        gridView.setAdapter(gridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent displayPicture =  new Intent(getApplicationContext(), DisplayPictureDetails.class);
                displayPicture.putExtra("imageId", db.getGalleryPictureId(mImages.get(i)));
                Log.d(TAG, "Intenti saadetud pictureId" + db.getGalleryPictureId(mImages.get(i)));
                startActivity(displayPicture);
            }
        });

        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    class GridAdapter extends BaseAdapter{
        private Context mContext;
        private ArrayList<Bitmap> mImages;

        public GridAdapter(Context context, ArrayList<Bitmap> images) {
            this.mContext = context;
            this.mImages = images;
        }

        @Override
        public int getCount() {
            Log.d(TAG, "getCount: " + mImages.size());
            return mImages.size();
        }

        @Override
        public Object getItem(int i) {
            return mImages.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
//           final Integer picture = mImages.get(i);

           if (view == null){
               final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
               view = layoutInflater.inflate(R.layout.gallery_image_view, null);

           }
            final ImageView imageView = view.findViewById(R.id.gallery_picture);

           gridView.setStretchMode(GridView.NO_STRETCH);
           imageView.setImageBitmap(mImages.get(i));

            Log.d(TAG, "In adapter setting URI" + mImages.get(i));



            return view;

//            TextView dummyTextView = new TextView(mContext);
//            dummyTextView.setText(String.valueOf(i));
//            return dummyTextView;
        }


    }

//    ArrayList<File> imageReader(File root) {
//        ArrayList<File> arrayList = new ArrayList<>();
//
//        File[] files = root.listFiles();
//        for (int i = 0)
//    }

    private void openGallery() {
//        Intent gallery = new Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
//        startActivityForResult(gallery,PICK_IMAGE);
        Intent intent = new Intent();
        intent.setType("image/*"); //, video/*
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent.createChooser(intent,"Select images"), PICK_IMAGE);


//        Intent galleryIntent = new Intent(Intent.EXTRA_ALLOW_MULTIPLE);
//        galleryIntent.setType("image/*, video/*");
//        if (galleryIntent.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(Intent.createChooser(galleryIntent, "Select File"), PICK_IMAGE);
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Here we get pictures that user selected
//        String filepath = null;
//        Bitmap bitmap;


        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                String[] filePathColumn = { MediaStore.Images.Media.DATA };
                imagesEncodedList = new ArrayList<>();
//                if(data.getData()!=null){
//
//                    Uri mImageUri=data.getData();
//
//                    // Get the cursor
//                    Cursor cursor = getContentResolver().query(mImageUri,
//                            filePathColumn, null, null, null);
//                    // Move to first row
//                    cursor.moveToFirst();
//
//                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                    imageEncoded  = cursor.getString(columnIndex);
//                    cursor.close();
//
//
//
//                    Log.d(TAG, "1" + imageEncoded);
//                    mImages.add(imageEncoded);
//                    db.addGalleryPictures(tripId, imageEncoded);
//
//
//
//
////                    galleryAdapter = new GalleryAdapter(getApplicationContext(),mArrayUri);
////                    gvGallery.setAdapter(galleryAdapter);
////                    gvGallery.setVerticalSpacing(gvGallery.getHorizontalSpacing());
////                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) gvGallery
////                            .getLayoutParams();
////                    mlp.setMargins(0, gvGallery.getHorizontalSpacing(), 0, 0);
//
//                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded  = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                            cursor.close();


                            Log.d(TAG, "3: " + imageEncoded);
//                            mImages.add(imageEncoded);
                            db.addGalleryPictures(tripId,imageEncoded);
                            pictureBitmaps.add(getBitmap(new File(imageEncoded)));

                            GridAdapter gridAdapter = new GridAdapter(this, pictureBitmaps);
                            gridView.setAdapter(gridAdapter);


//                            galleryAdapter = new GalleryAdapter(getApplicationContext(),mArrayUri);
//                            gvGallery.setAdapter(galleryAdapter);
//                            gvGallery.setVerticalSpacing(gvGallery.getHorizontalSpacing());
//                            ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) gvGallery
//                                    .getLayoutParams();
//                            mlp.setMargins(0, gvGallery.getHorizontalSpacing(), 0, 0);

                        }
                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                    }
//                }
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }


        super.onActivityResult(requestCode, resultCode, data);
    }




        @Override
        public boolean onCreateOptionsMenu (Menu menu){
            getMenuInflater().inflate(R.menu.gallery_menu, menu);
            return super.onCreateOptionsMenu(menu);
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            // Here should be implemented onClick actions


            int id = item.getItemId();

            switch (id) {
                case R.id.add_gallery_picture:
                    openGallery();
                    return true;
            }


            return super.onOptionsItemSelected(item);

        }


        private ArrayList<Bitmap> initializePicArray(){
            ArrayList<Bitmap> temp = new ArrayList<>();
            mImages =  db.getAllGalleryPictures(this.tripId);


            for (int i = 0; i < mImages.size(); i++){
                File imgFile = new File(mImages.get(i));
                temp.add(getBitmap(imgFile));
            }
            return temp;
        }

    private Bitmap getBitmap(File imgFile) {
        Bitmap myBitmap = null;
        String[] permissions = new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE};
        if (imgFile.exists()) {
            final int MyVersion = Build.VERSION.SDK_INT;
            if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (!checkIfHavePermission()) {
                    ActivityCompat.requestPermissions(this, permissions, 1);
                } else {
//                    myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath()); //TODO: Have to check if we even need this part

                }

                myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath()); // TODO: Have to check it doesn't crash without permission
            }
        }

        return myBitmap;
    }

    private boolean checkIfHavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

}





