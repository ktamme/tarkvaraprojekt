package com.example.Tripal;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;



public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{
    private static final String TAG = "RecycleViewAdapter";

    private ArrayList<String> mImageNames = new ArrayList<>();
    private ArrayList<Bitmap> mImages = new ArrayList<>();
    private Context mContext;
    private OnTripListener mOnTripListener;

    public RecyclerViewAdapter(Context context, ArrayList<String> imageNames, ArrayList<Bitmap> images, OnTripListener onTripListener) {
        mImageNames = imageNames;
        mImages = images;
        mContext = context;
        this.mOnTripListener = onTripListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem,parent,false);
        ViewHolder holder = new ViewHolder(view, mOnTripListener);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG,"onBindViewHolder: called.");


            Glide.with(mContext)
                    .asBitmap()
                    .load(mImages.get(position))
                    .error(R.drawable.landscape_icon)
                    .into(holder.image);

        Log.d(TAG, "Adaptris pildi laadimine: " + mImages.get(position));
        holder.imageName.setText(mImageNames.get(position));


    }
    @Override
    public int getItemCount() {
        return mImageNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView image;
        TextView imageName;
        RelativeLayout parentLayout;

        OnTripListener onTripListener;

        public ViewHolder(View itemView, OnTripListener onTripListener) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            imageName = itemView.findViewById(R.id.image_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            this.onTripListener = onTripListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onTripListener.onTripClick(getAdapterPosition());
        }
    }

    public interface OnTripListener {
        void onTripClick(int position);
    }


}





