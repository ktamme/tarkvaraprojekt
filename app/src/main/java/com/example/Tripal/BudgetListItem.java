package com.example.Tripal;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class BudgetListItem extends AppCompatActivity implements BudgetRecyclerViewAdapter.OnItemListener {

    private static final String TAG = "BUDGETLISTITEM";
    private Integer tripId;
    private Integer categoryId;
//    private MenuItem addExpence;
    private Toolbar toolbar;
    private TextView mTitle;
    private DBHandler db;
    private Button addExpence;


    private RecyclerView.Adapter adapter;

    private ArrayList<String> mItemName = new ArrayList<>();
    private ArrayList<Double> mItemPrice = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.budget_item_layout);
        db = new DBHandler(this,null,null);
        Intent intent = getIntent();
        this.tripId =  intent.getIntExtra("TripId", -1);
        String categoryName = intent.getStringExtra("tripCategoryName");
        this.categoryId =  intent.getIntExtra("tripCategoryId", -1);

        toolbar = (Toolbar) findViewById(R.id.budget_item_toolbar);
        mTitle = toolbar.findViewById(R.id.toolbar_title);
        ImageView backspace = toolbar.findViewById(R.id.tool_bar_backspace);
//        mTitle.setText(db.getCategoryName(tripId, categoryId));
        mTitle.setText(categoryName);
        addExpence = findViewById(R.id.btn_add_expense);
//        addExpence = findViewById(R.id.add_expense);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        addExpenceListener();
        this.adapter = initBudgetRecyclerView();


        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.budget_item_menu,menu);
//        return super.onCreateOptionsMenu(menu);
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//
//        switch (id){
//            case R.id.add_expense:
//                final AlertDialog.Builder builder = new AlertDialog.Builder(BudgetListItem.this);
//                View mView = getLayoutInflater().inflate(R.layout.add_item_popup_box, null);
//
//                final EditText inputName = mView.findViewById(R.id.popup_box_item_name);
//                final EditText inputPrice = mView.findViewById(R.id.popup_box_item_price);
//                final Button addItem = mView.findViewById(R.id.btn_add_item);
//                builder.setView(mView);
//                final AlertDialog dialog = builder.create();
//
//                addItem.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Fragment prev = getSupportFragmentManager().findFragmentByTag("fragment_dialog");
//                        if (!inputName.getText().toString().isEmpty() && !inputPrice.getText().toString().isEmpty()){
//                            try
//                            {
//                                String inputNameBuffer = inputName.getText().toString();
//                                Double inputPriceBuffer = Double.parseDouble(inputPrice.getText().toString());
//
//                                db.addTripBudgetItem(inputName.getText().toString(),inputPrice.getText().toString(),tripId,categoryId);
//                                mItemName.add(inputNameBuffer);
//                                mItemPrice.add(inputPriceBuffer);
//
//                                adapter.notifyItemInserted(mItemName.indexOf(inputName.getText().toString()));
//                            }catch (Exception e){
//                                Toast.makeText(BudgetListItem.this, "Please enter numbers as price",
//                                        Toast.LENGTH_LONG).show();
//                            }
//                        }else{
//                            Toast.makeText(BudgetListItem.this, "No trip entered!",
//                                    Toast.LENGTH_LONG).show();
//
//                        }
//
//                        dialog.dismiss();
//
//                        if (prev != null) {
//                            DialogFragment df = (DialogFragment) prev;
//                            df.dismiss();
//                        }
//                    }
//                });
//                builder.setView(mView);
//                if (!BudgetListItem.this.isFinishing()) {
//                    dialog.show();
//                }
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void addExpenceListener () {

        addExpence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(BudgetListItem.this);
                View mView = getLayoutInflater().inflate(R.layout.add_item_popup_box, null);

                final EditText inputName = mView.findViewById(R.id.popup_box_item_name);
                final EditText inputPrice = mView.findViewById(R.id.popup_box_item_price);
                final Button addItem = mView.findViewById(R.id.btn_add_item);
                builder.setView(mView);
                final AlertDialog dialog = builder.create();

                addItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Fragment prev = getSupportFragmentManager().findFragmentByTag("fragment_dialog");
                        if (!inputName.getText().toString().isEmpty() && !inputPrice.getText().toString().isEmpty()){
                            try
                            {
                                String inputNameBuffer = inputName.getText().toString();
                                Double inputPriceBuffer = Double.parseDouble(inputPrice.getText().toString());

                                db.addTripBudgetItem(inputName.getText().toString(),inputPrice.getText().toString(),tripId,categoryId);
                                mItemName.add(inputNameBuffer);
                                mItemPrice.add(inputPriceBuffer);

                                adapter.notifyItemInserted(mItemName.indexOf(inputName.getText().toString()));
                            }catch (Exception e){
                                Toast.makeText(BudgetListItem.this, "Please enter numbers as price",
                                        Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(BudgetListItem.this, "No trip entered!",
                                    Toast.LENGTH_LONG).show();

                        }

                        dialog.dismiss();

                        if (prev != null) {
                            DialogFragment df = (DialogFragment) prev;
                            df.dismiss();
                        }
                    }
                });
                builder.setView(mView);
                if (!BudgetListItem.this.isFinishing()) {
                    dialog.show();
                }
            }
        });

    }




    private RecyclerView.Adapter initBudgetRecyclerView (){
        Log.d(TAG, "initRecyclerView: init recyclerview.");

        Map map = db.getTripBudgetItems(tripId,categoryId);


        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();
        while(itr.hasNext()){
            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry=(Map.Entry)itr.next();
            mItemName.add(entry.getKey().toString());
            mItemPrice.add(Double.valueOf(entry.getValue().toString()));

        }

        RecyclerView recyclerView = findViewById(R.id.budget_list_item_recycler_view);
        BudgetRecyclerViewAdapter adapter = new BudgetRecyclerViewAdapter(mItemName,mItemPrice, this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        return adapter;
    }


    @Override
    public void onItemClick(int position) {

    }
}
