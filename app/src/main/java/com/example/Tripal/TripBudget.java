package com.example.Tripal;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;


import java.util.ArrayList;

public class TripBudget extends AppCompatActivity implements BudgetRecyclerViewAdapter.OnItemListener {

    private static final String TAG = "TripBudget";

    private ArrayList<String> mCategoryNames = new ArrayList<>();
    private ArrayList<Double> mCategorySums = new ArrayList<>();
    BudgetRecyclerViewAdapter adapter;
    private TextView tripSum;
    private Button addCategory;
    private String m_Text;
    private DBHandler db;
    private Integer tripId;
    private Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.budget_layout);
        tripSum = findViewById(R.id.trip_sum);
        addCategory = findViewById(R.id.btn_add_category);
        db = new DBHandler(this,null,null);


        Intent intent = getIntent();
        this.tripId = intent.getIntExtra("TripId", -1);
        if (db.getAllTripBudgetCategories(tripId).isEmpty()){
            db.initialiseBudgetCategories(tripId);
        }


        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.budget_category_tool_bar);
        ImageView backspace = toolbar.findViewById(R.id.tool_bar_backspace);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Budget");


        tripSum.setText(String.valueOf(db.getTripSum(tripId) + " €" ));
        this.adapter = initBudgetRecyclerView();
        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(TripBudget.this);
                builder.setTitle("Add category");
                final EditText input = new EditText(TripBudget.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);



                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        m_Text = input.getText().toString();
                        db.addTripBudgetCategory(m_Text,tripId);
                        mCategoryNames.add(m_Text);
                        mCategorySums.add(0.0);
                        adapter.notifyItemInserted(mCategoryNames.indexOf(m_Text));
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                final AlertDialog dialog = builder.create();

                if (!TripBudget.this.isFinishing()) {
                    dialog.show();
                }
            }
        });

        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadBudgetRecyclerView();
    }

    @Override
    public void onItemClick(int position) {
        Log.d(TAG, "onItemClick: clicked");
        Intent intent =  new Intent(this,BudgetListItem.class);
        intent.putExtra("TripId", tripId);
        intent.putExtra("tripCategoryName", mCategoryNames.get(position));
        intent.putExtra("tripCategoryId", position);
        //FIXME: Can't expect that position in list is equivalent to category id
        startActivity(intent);
    }


    private BudgetRecyclerViewAdapter initBudgetRecyclerView(){
        Log.d(TAG, "InitBudgetRecyclerView");
        ArrayList arrayList;
        arrayList = db.getAllTripBudgetCategories(tripId);
        mCategoryNames = arrayList;

        for (int i = 0; i <mCategoryNames.size();i++){
            mCategorySums.add(db.getCategorySum(tripId,i));

        }

        String temp = " " + arrayList.size();
        Log.d(TAG,temp );


        RecyclerView recyclerView = findViewById(R.id.budget_recycler_view);
        BudgetRecyclerViewAdapter adapter = new BudgetRecyclerViewAdapter(mCategoryNames,mCategorySums, this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        return adapter;

    }

    private void reloadBudgetRecyclerView(){
        Log.d(TAG, "ReloadBudgetRecyclerView");
        ArrayList arrayList ;
        arrayList = db.getAllTripBudgetCategories(tripId);
        mCategoryNames.clear();
        mCategorySums.clear();
        mCategoryNames = arrayList;

        for (int i = 0; i < mCategoryNames.size();i++){
            mCategorySums.add(db.getCategorySum(tripId,i));

        }
        tripSum.setText(String.valueOf(db.getTripSum(tripId) + " €" ));



        RecyclerView recyclerView = findViewById(R.id.budget_recycler_view);
        BudgetRecyclerViewAdapter adapter = new BudgetRecyclerViewAdapter(mCategoryNames,mCategorySums, this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));



    }


}
