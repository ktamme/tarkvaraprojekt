package com.example.Tripal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class NoteContent extends AppCompatActivity {
    private static final String TAG = "NoteContent";
    private Button save;
    private EditText context;
    private String strContext;
    private Integer noteId;
    private Note note;
    private DBHandler db;

    public String getNoteContext () { return context.getText().toString(); }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.note_content);
        context = findViewById(R.id.note_context_field);
        this.db = new DBHandler(this, null, null);
        Intent intent = getIntent();
        this.noteId = intent.getIntExtra("NoteID", -1);
        if (noteId == -1) { this.finishAffinity(); }         //  Kui ei saanud NoteId'd
        super.onCreate(savedInstanceState);
        ImageView backspace = findViewById(R.id.note_content_backspace);
        String noteCont = db.getNoteContext(noteId);
        if (!noteCont.equals("null")) {
            context.setText(noteCont);
        }

        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Log.d(TAG, "NoteContent : open");
        save = (Button) findViewById(R.id.save_note);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view) {
                switch (view.getId()) {
                    case R.id.save_note:
                        Log.d(TAG, "NoteContent : Save Clicked");
                        Intent intent = new Intent(NoteContent.this, NoteMainAct.class);
                        strContext = getNoteContext();
                        db.updateNoteContent(strContext, noteId.toString());
                        Log.d(TAG, "NoteContent : saved");
                        Toast.makeText(NoteContent.this, "Note has been saved", Toast.LENGTH_SHORT).show();
                        context.setText(strContext);
                }
            }
        });


    }
}